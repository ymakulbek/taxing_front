export default class NCALayer {
  /**
   * NCALayer constructor
   * @param store
   * @param url
   */
  constructor (store, url = 'wss://127.0.0.1:13579/') {
    this.websocket = new WebSocket(url)

    this.websocket.onerror = event =>
      store.commit('error/setSystemBar', { data: event, message: 'ncalayer_error' })
  }

  sign (data) {

  }
}
