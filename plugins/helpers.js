const helpers = {
  /**
   * Fetch params
   * @param page
   * @param itemsPerPage
   * @param sortBy
   * @param sortDesc
   * @returns {string}
   */
  resourceFetchParams ({ page = 1, itemsPerPage = 15, sortBy = [], sortDesc = [] }) {
    const params = { page, itemsPerPage }

    sortBy.length && (
      params.sortBy = sortBy.map((item, index) => sortDesc[index] ? `${item}:desc` : `${item}`).join('&')
    )

    return Object.keys(params).map(key => `${key}=${params[key]}`).join('&')
  }
}

/**
 * Inject helpers
 * @param app
 * @param inject
 */
export default ({ app }, inject) => {
  inject('helpers', helpers)
}
