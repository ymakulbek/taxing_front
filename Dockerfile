FROM node:alpine

RUN mkdir /code

WORKDIR /code

COPY . .

RUN apk add g++ python make && npm rebuild node-sass
