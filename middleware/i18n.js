import { isEmpty } from 'lodash'

/**
 * Set language
 * @param axios
 * @param lang
 */
const setLanguage = (axios, lang) => {
  axios.setHeader('Accept-Language', lang)
  process.client && document.querySelector('html').setAttribute('lang', lang)
}

/**
 * Middleware
 * @param app
 * @param $axios
 * @returns {Promise<void>}
 */
export default async ({ app, $axios }) => {
  const i18n = app.i18n
  const lang = i18n.locale

  if (!isEmpty(i18n.getLocaleMessage(lang))) {
    return setLanguage($axios, lang)
  }

  const translation = await import(/* webpackChunkName: "lang-[request]" */ `@/lang/${lang}.json`)

  i18n.setLocaleMessage(lang, translation)
  setLanguage($axios, lang)
}
